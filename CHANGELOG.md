# Changelog

## v3.0.5 (2025-02-06)

### Fixed

- Updated base image to `eclipse-temurin:21.0.6_7`.

## v3.0.4 (2024-10-25)

### Fixed

- Updated base image to `eclipse-temurin:21.0.5_11`.

## v3.0.3 (2024-09-29)

### Fixed

- Fixed CI pipeline to properly update image `annotations` to be correct for this
  project. Previously, the `annotations` from the base `eclipse-temurin` image
  were cascading to this image.

## v3.0.2 (2024-07-23)

### Changed

- Updated image to `eclipse-temurin:21.0.4`.

### Miscellaneous

- Updated to Renovate config presets v1.1.0.

## v3.0.1 (2024-04-24)

### Changed

- Updated image to `eclipse-temurin:21.0.3`.

### Miscellaneous

- Updated Renovate config to combine container image digest changes.

## v3.0.0 (2024-02-21)

### Changed

- BREAKING: Updated to `eclipse-temurin:21.0.2`.

## v2.1.2 (2024-02-20)

### Fixed

- Updated to `eclipse-temurin:17.0.10`.

### Miscellaneous

- Updated Renovate config to use use new
  [presets](https://gitlab.com/gitlab-ci-utils/renovate-config).
  project and built-in `javaLTSVersions` preset (allowing major Docker
  releases). (#13, #14)

## v2.1.1 (2023-09-17)

### Fixed

- Updated to `eclipse-temurin:17.0.8`.
- Update Renovate config for v36.

## v2.1.0 (2023-03-30)

### Changed

- Added standard set of `LABEL`s to image. (#10)
- Update to `eclipse-temurin:17.0.6_10-jre-alpine` image.

### Miscellaneous

- Fixed `renovate` config to properly update `eclipse-temurin` images. (#11)

## v2.0.1 (2022-10-17)

### Fixed

- Updated container image license `LABEL` to properly comply with the
  [OCI Image Format Specification](https://github.com/opencontainers/image-spec/).
  (#9)

## v2.0.0 (2022-10-16)

### Changed

- BREAKING: Updated container images used for the build and final release since
  the `openjdk` image are now
  [deprecated](https://hub.docker.com/_/openjdk). The project now uses
  [eclipse-temurin](https://hub.docker.com/_/eclipse-temurin) images, the OS
  was changed from Debian (slim) to Alpine, and the Java versionw was updated
  from v11 to v17. This has resulted in a smaller image and shorter runtimes.
  (#6)
  - These changes should only be BREAKING if the container image is used for
    anything other than executing the `/sokrates/analyze.sh` script.
- BREAKING: Updated the README to specify that this project is licensed under
  the [Apache-2 license](./LICENSE), but the built container image is licensed
  under the [AGPL-3.0 license](https://opensource.org/licenses/AGPL-3.0). The
  container image was also updated with `LABEL`s that specify the license. (#7)

### Miscellaneous

- Updated the CI pipeline to have long-running jobs use GitLab shared `medium`
  sized runners and optimized `needs`, both to reduce pipeline execution times.
  (#5, #8)

## v1.0.0 (2022-05-08)

Initial release
