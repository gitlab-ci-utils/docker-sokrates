FROM eclipse-temurin:21.0.6_7-jdk-alpine@sha256:cafcfad1d9d3b6e7dd983fa367f085ca1c846ce792da59bcb420ac4424296d56 as build

WORKDIR /app

# hadolint ignore=DL3018
RUN apk update && \
  apk add --no-cache git maven && \
  rm -rf /var/cache/apk/* && \
  git clone https://github.com/zeljkoobrenovic/sokrates.git

WORKDIR /app/sokrates

RUN mvn clean install && \
  # Built .jar has the version in the name, so copy to a
  # consistent name to simplify copying to final image.
  cp cli/target/cli-*-with-dependencies.jar cli/target/sokrates.jar

FROM eclipse-temurin:21.0.6_7-jre-alpine@sha256:4e9ab608d97796571b1d5bbcd1c9f430a89a5f03fe5aa6c093888ceb6756c502

COPY --from=build /app/sokrates/cli/target/sokrates.jar /app/sokrates/LICENSE /sokrates/
COPY image-files/ /sokrates/

# hadolint ignore=DL3018
RUN apk update && \
  apk add --no-cache graphviz && \
  rm -rf /var/cache/apk/* && \
  chmod +x /sokrates/analyze.sh

LABEL org.opencontainers.image.licenses="AGPL-3.0-only"
LABEL org.opencontainers.image.source="https://gitlab.com/gitlab-ci-utils/docker-sokrates"
LABEL org.opencontainers.image.title="docker-sokrates"
LABEL org.opencontainers.image.url="https://gitlab.com/gitlab-ci-utils/docker-sokrates"

ENTRYPOINT ["/sokrates/analyze.sh"]
