# Docker Sokrates

A container image to run the [Sokrates](https://www.sokrates.dev/) code examination tool. The image is based on the Java 17 [Eclipse Temurin](https://hub.docker.com/_/eclipse-temurin) Alpine images (see the [Dockerfile](./Dockerfile) for the specific images).

## Usage

### Docker CLI

An example illustrating how to analyze the current working directory is shown below. Note `sokrates` titles reports with the name of the containing folder, so the `/app-name` folder shown below should reflect the appropriate name (in both locations).

```sh
docker run --rm -it -v ${pwd}:/app-name -w /app-name registry.gitlab.com/gitlab-ci-utils/docker-sokrates:latest
```

This saves reports in the `./_sokrates` folder, and the HTML report is available at `./_sokrates/reports/html/index.html`.

### GitLab CI usage

The following is an example illustrating how to analyze a project in GitLab CI.

```yaml
sokrates:
  image:
    name: registry.gitlab.com/gitlab-ci-utils/docker-sokrates:latest
    entrypoint: [""]
  stage: test
  needs: []
  allow_failure: true
  script:
    - /sokrates/analyze.sh
    # Fail job if the results folder is empty since Sokrates does
    # not fail with a non-zero exit code on error.
    - if [ -z "$(ls -A _sokrates/)" ]; then exit 2; fi
  artifacts:
    paths:
      - _sokrates/
```

This saves the entire `_sokrates` results folder, and as noted previously the HTML report can be found at `_sokrates/reports/html/index.html`.

## License

This project, including the `Dockerfile` and `README` instructions, is licensed under the [Apache-2 license](./LICENSE). The built container image, as pulled from the container registry (`registry.gitlab.com/gitlab-ci-utils/docker-sokrates`), is licensed under the [AGPL-3.0 license](https://opensource.org/licenses/AGPL-3.0). The container image contains a compiled, but unmodified, build of the [Sokrates](https://github.com/zeljkoobrenovic/sokrates/) application, which is also licensed under the [AGPL-3.0 license](https://github.com/zeljkoobrenovic/sokrates/blob/master/LICENSE), and a copy of this LICENSE and README with links to the source code is available in the container image in the `/sokrates/` directory (the source is left off of the container image simply to reduce the size of the image).
