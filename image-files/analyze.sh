#!/bin/sh

# Run Sokrates analysis per https://www.sokrates.dev/
java -jar /sokrates/sokrates.jar extractGitHistory
java -jar /sokrates/sokrates.jar init
java -jar /sokrates/sokrates.jar generateReports
